const webpack = require('webpack'),
  path = require('path'),
  UglifyJsPlugin = require('uglifyjs-webpack-plugin'),
  HtmlWebpackPlugin = require('html-webpack-plugin');

let config = {
  entry: './src/app/index.js',
  output: {
    path: path.resolve(__dirname, '/dist'), // Folder to store generated bundle
    filename: 'bundle.js' // Name of generated bundle after build


  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: "babel-loader"
    }]
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin(),
    new HtmlWebpackPlugin({

      template: path.resolve(__dirname, './src/public/index.html'),
      inject: 'body'
    })
  ],
  devServer: {
    contentBase: path.resolve(__dirname, './src/public'),
    historyApiFallback: true,
    inline: true,
    open: true,
    port: 3000
  },
  devtool: 'eval-source-malp'
}

module.exports = config;
