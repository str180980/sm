
 

let newImage = {

  loadFile(reader, file) {

      this.canvasHTML =
        `<canvas id='${slideIndex}'
        class='mySlides'
        style='border: 0px'></canvas>`;

      $('.mainImage').append(this.canvasHTML);

      this.canvas = $(`.mainImage`).find(
        `canvas[id="${slideIndex}"]`)[0];
      this.ctx = this.canvas.getContext('2d');

      $('button[id="zoomIn"]').attr('class', slideIndex);
      $('button[id="zoomOut"]').attr('class', slideIndex);

      if (file) {
        reader.readAsDataURL(file);
        reader.addEventListener('load', () => {
          this.reader = reader.result;
          Promise.all([
              this.createImag('mainIMG', loadingMainImage,
                this),
              this.createImag('galleryIMG', loadingGalleryImage,
                slideIndex
              )
            ])
            .then((img) => {
              this.displayFile(reader, file);
            })
            .catch(err => {
              throw err;
            })
        });
      }
    },
    displayFile(reader, file) {
      let self = this;
      //   this.angleDGR = angleInDegrees;
      $(`.allImages > .row > .column[id="${slideIndex}"]`).on(
        'click',
        function() {
          currentSlide = $(this).attr('id');
          $('button[id="zoomIn"]').attr('class', currentSlide);
          $('button[id="zoomOut"]').attr('class', currentSlide);

          showSlides(currentSlide);
        });
      showSlides(slideIndex);

    },
    createImag(imageType, loadCallback, args) {
      this[imageType] = new Image;
      this[imageType].src = this.reader;
      return new Promise((resolve, reject) => {
        this[imageType].onload = loadCallback(this[imageType],
          args, resolve);
      });
    }
}
