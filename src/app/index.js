   $(function() {

     let slideIndex = 0,
       currentSlide = 1,
       imgInstances = [];
   let angleInDegrees = 0;



     function imageCLASS() {

       let zoomIn = 1,
         zoomOut = 1;

       function loadingGalleryImage(img, id, resolve) {
         let blabla = $('.allImages > .row');

        // img.style.width = '100%';
         let galleryImg =
           `<div class="column" id="${id}" style="height:100%;"></div>`;
         img.className = 'demo cursor';
         img.style.height = '100%';
            img.style.width = '100%';

         $(blabla).append(galleryImg);
         $(`.allImages > .row > .column[id="${slideIndex}"]`).append(
           img);
         resolve('cool');

       }

       function loadingMainImage(img, obj, resolve) {
         console.log('RRRRRRRR IMAGEE');
         obj.canvas.width = img.width;
         obj.canvas.height = img.height;

         obj.ctx.drawImage(img, 0, 0,
           obj.canvas.width,
           obj.canvas.height);
         resolve('cool');

       }

       function drawRotated(degrees, obj) {
         console.log('THIS ARE DEGREEES');
         console.log(degrees);


         if (degrees == 90 || degrees == 270) {

           obj.canvas.width = obj.mainIMG.height;
           obj.canvas.height = obj.mainIMG.width;
         } else {
           obj.canvas.width = obj.mainIMG.width;
           obj.canvas.height = obj.mainIMG.height;
         }

         // obj.ctx.clearRect(0, 0, obj.canvas.width, obj.canvas.height);
         if (degrees == 90 || degrees == 270) {
           obj.ctx.translate(obj.mainIMG.height / 2, obj.mainIMG.width / 2);
         } else {
           obj.ctx.translate(obj.mainIMG.width / 2, obj.mainIMG.height / 2);
         }
         obj.ctx.rotate(degrees * Math.PI / 180);
         // obj.ctx.save();
         obj.ctx.translate(-obj.mainIMG.width / 2, -obj.mainIMG.height / 2);
         // obj.canvas.width *= 0.7;
         //   obj.canvas.height *= 0.7;

         obj.ctx.drawImage(obj.mainIMG, 0, 0, obj.mainIMG.width, obj.mainIMG
           .height);

              obj.mainIMG.src = obj.canvas.toDataURL();
       }

       function zoomImg(degrees, obj, scale) {

         obj.ctx.scale(scale, scale);

         obj.canvas.width *= scale;
         obj.canvas.height *= scale;
      //   obj.mainIMG.width *= scale;
        // obj.mainIMG.height *= scale;

         obj.ctx.drawImage(obj.mainIMG, 0, 0);
            obj.mainIMG.src = obj.canvas.toDataURL();
         /*if (degrees == 90 || degrees == 270) {
           obj.ctx.translate(obj.mainIMG.height / 2, obj.mainIMG.width /
             2);
         } else {
           obj.ctx.translate(obj.mainIMG.width / 2, obj.mainIMG.height /
             2);
         }

         obj.ctx.rotate(degrees * Math.PI / 180);
         obj.ctx.translate(-obj.mainIMG.width / 2, -
           obj.mainIMG.height /
           2);

         if (degrees == 90 || degrees == 270) {
           obj.ctx.drawImage(obj.mainIMG, 0, 0, obj.canvas.height, obj.canvas
             .width);
         } else
           obj.ctx.drawImage(obj.mainIMG, 0, 0, obj.canvas.width, obj.canvas
             .height);*/
       }


       return {

         loadFile(reader, file) {
           this.rdr = reader;
           this.canvasHTML =
             `<canvas id='${slideIndex}'
            class='mySlides'
            style='border: 0px'></canvas>`;

           $('.mainImage').append(this.canvasHTML);

           this.canvas = $(`.mainImage`).find(
             `canvas[id="${slideIndex}"]`)[0];
           this.ctx = this.canvas.getContext('2d');

           $('button[id="zoomIn"]').attr('class', slideIndex);
           $('button[id="zoomOut"]').attr('class', slideIndex);

           if (file) {
             reader.readAsDataURL(file);
             reader.addEventListener('load', () => {
               this.reader = reader.result;
               console.log('READER RESUT');
               console.log(reader.result);
               Promise.all([
                   this.createImag('mainIMG', loadingMainImage,
                     this, true),
                   this.createImag('galleryIMG', loadingGalleryImage,
                     slideIndex, true
                   )
                 ])
                 .then((img) => {
                   this.displayFile(reader);
                 })
                 .catch(err => {
                  console.log(err);
                 })
             });
           }
         },
         displayFile(reader) {
           let self = this;
           //   this.angleDGR = angleInDegrees;
           $(`.allImages > .row > .column[id="${slideIndex}"]`).on(
             'click',
             function() {
               currentSlide = $(this).attr('id');
               $('button[id="zoomIn"]').attr('class', currentSlide);
               $('button[id="zoomOut"]').attr('class', currentSlide);

               self.showSlides(currentSlide);
             });
           this.showSlides(slideIndex);

         },
         createImag(imageType, loadCallback, args, imgState) {
           console.log('CREATE MAIN IMGGG');
           //   if (imgState === true)


           /*
            if (imgState === false) {
              loadCallback(this[imageType],
                args, false);
            }*/
           return new Promise((resolve, reject) => {
             this[imageType] = new Image;
             this[imageType].src = this.reader;
           this[imageType].addEventListener("error", () => {
             console.log("THIS IS ERRROR LOAD");
            reject("ERROR LOAD");
          });
          //   this[imageType].onerror = reject("ERROR LOAD");
             this[imageType].addEventListener("load", loadCallback(this[imageType],
               args, resolve));


           });
         },
         showSlides(n) {

           let slides = $(".mySlides"),
             galleryImg = $('.demo');

           for (let i = 0; i < slides.length; i++) {
             console.log('HERE IS SLIDES');
             console.log(slides[i]);
             slides[i].style.display = "none";
           }
           /*   for (let i = 0; i < galleryImg.length; i++) {
                galleryImg[i].className = galleryImg[i].className.replace(
                  " active", "");
              }*/
           slides[n - 1].style.display = "block";
           //   galleryImg[n - 1].className += " active";
         },
         rotate(direction) {
           if (direction === 'right') {
             angleInDegrees = 90;
             if (angleInDegrees > 360)
               angleInDegrees = 90;
           } else {
             angleInDegrees = -90;
             if (angleInDegrees < 0)
               angleInDegrees = 270;
           }

           drawRotated(angleInDegrees, this);
         },
         scaleImg(scaleCoef) {

           zoomImg(angleInDegrees, this, scaleCoef);

         }
       }
     }

     $('input[type="file"]').on('change', function() {
       let fReader = new FileReader(),
         file = $('#bla')[0].files[0],
         img = Object.create(imageCLASS());

       console.log('this is FILEEE');
       console.log(file);
       img.loadFile(fReader, file);
       slideIndex++;
       currentSlide = slideIndex;
       imgInstances[slideIndex] = img;
     });



     $('a[class="downloadFL"]').on('click', function() {
       this.href = imgInstances[currentSlide].canvas.toDataURL();
       this.download = 'blabla';
     });

     $('button[id="rotateR"]').on('click', function() {

       imgInstances[currentSlide].rotate('right');

     });
     $('button[id="rotateL"]').on('click', function() {

       imgInstances[currentSlide].rotate('left');

     });
     $('button[id="zoomIn"]').on('click', function() {

       let hmhmh = imgInstances[currentSlide].canvas.toDataURL();
      imgInstances[currentSlide].mainIMG.src = hmhmh;

       angleInDegrees = 0;

     });
     $('button[id="mirror"]').on('click', function() {
       imgInstances[currentSlide].canvas.width = imgInstances[currentSlide].mainIMG.width;
          imgInstances[currentSlide].canvas.height = imgInstances[currentSlide].mainIMG.height;
       imgInstances[currentSlide].ctx.scale(-1, 1);

       imgInstances[currentSlide].ctx.drawImage(imgInstances[currentSlide]
         .mainIMG, 0, 0, imgInstances[currentSlide].canvas.width * (-1),
         imgInstances[currentSlide].canvas.height);
         let hmhmh = imgInstances[currentSlide].canvas.toDataURL();
        imgInstances[currentSlide].mainIMG.src = hmhmh;

     });
     $('button[id="save"]').on('click', function() {

       imgInstances[currentSlide].rotate('left');

     });

     $('button[id="zoomOut"]').on('click', function() {

       imgInstances[currentSlide].scaleImg(0.90);

     });


     $('a[class="prev"]').on('click', () => {
       currentSlide === 1 ? currentSlide = slideIndex : currentSlide -= 1;
       imgInstances[currentSlide].showSlides(currentSlide);
     });

     $('a[class="next"]').on('click', () => {

       currentSlide === slideIndex ? currentSlide = 1 : currentSlide += 1;
       imgInstances[currentSlide].showSlides(currentSlide);
     });

   });
